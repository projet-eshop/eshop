import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { RowProduct } from "./RowProduct";
import { Transport } from "./Transport";
import { User } from "./User";

@Entity()

export class Order {

    @PrimaryGeneratedColumn()
    id: number;
    @Column({type: 'date'})
    dateCrea: Date;
    
    @Column()
    statusDelivered: number;

    @Column()
    priceTotal: number;
    

    @ManyToOne(() => User, user => user.orders)
    user: User;

    @ManyToOne(() => Transport, transport => transport.orders)
    transport: Transport; 

    @OneToMany(() =>RowProduct , rowProduct => rowProduct.order,{cascade:true})
    rowProducts: RowProduct[];

}

