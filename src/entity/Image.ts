import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Product } from "./Product";

@Entity()
export class Image {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    fileName: string;

    @ManyToOne(() => Product, product => product.images)
    product: Product;

    get url(): string {return '/uploads/' + this.fileName}
    get thumbnail(): string {return '/uploads/thumbnails/' + this.fileName}

    toJSON(){return {...this,url:this.url,thumbnail:this.thumbnail}}
}