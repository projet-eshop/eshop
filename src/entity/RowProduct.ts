import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Order } from "./Order";
import { Product } from "./Product";
import { User } from "./User";

@Entity()
export class RowProduct {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    price: number;

    @Column()
    quantity: number;

    @ManyToOne(()=> Order, order => order.rowProducts)
    order: Order;

    @ManyToOne(()=> Product, product => product.rowProducts)
    product: Product;

    @ManyToOne(()=> User, user => user.shoppingCart)
    user: User;

}