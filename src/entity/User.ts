import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Comment } from "./Comment";
import { Order } from "./Order";
import { RowProduct } from "./RowProduct";
import Joi from "joi";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number; 
    @Column()
    name: string;
    @Column()
    firstName: string;
    @Column()
    password: string;
    @Column()
    email: string;
    @Column()
    role: string;
    @Column({ type: 'date' })
    birthdate: Date;
    @Column()
    address: string;

    @OneToMany(() => Comment, comment => comment.user)
    comments: Comment[];

    @OneToMany(() => RowProduct, rowProduct => rowProduct.user)
    shoppingCart: RowProduct[];

    @OneToMany(() => Order, order => order.user)
    orders: Order[];
}

export const userSchema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.number().min(8).required(),
    name: Joi.string().min(4),
    firstName: Joi.string().min(4),
    birthdate: Joi.date(),
    address: Joi.string()
}
);