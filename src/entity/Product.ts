import { Column, Entity, JoinTable, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Comment } from "./Comment";
import { Image } from "./Image";
import { RowProduct } from "./RowProduct";
import Joi from 'joi';



@Entity()
export class Product {

   @PrimaryGeneratedColumn()
   id: number;
   @Column()
   name: string;
   @Column()
   description: string;
   @Column()
   price: number;
  


   @OneToMany(() => Comment, comment => comment.product, {  onDelete: 'CASCADE',cascade:true  })
   @JoinTable()
   comments: Comment[];

   @OneToMany(() => RowProduct, rowProduct => rowProduct.product, { onDelete: 'CASCADE' ,cascade:true })
   @JoinTable()
   rowProducts: RowProduct[];

   @OneToMany(() => Image, image => image.product, { onDelete: 'CASCADE',cascade:true })
   @JoinTable()
   images: Image[];

}

export const productSchema = Joi.object({
   name: Joi.string().min(5).max(50).required(),
   description: Joi.string().min(20).max(1000).required(),
   price: Joi.number().min(1).max(50000).required(),
});
