import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./User";
import { Product } from "./Product";
import Joi from "joi";


@Entity()
export class Comment {

    @PrimaryGeneratedColumn()
    id:number;
    @Column({default:null})
    ratingNumber:number;
    @Column()
    description:string;
    @Column({type:'date'})
    date:Date;
    @ManyToOne(() => User, user => user.comments)
    user: User; 
    @ManyToOne(() => Product, product => product.comments)
    product: Product;

}

export const commentSchema = Joi.object({
    description: Joi.string().min(3).max(300),
    ratingNumber: Joi.number(),
    date: Joi.date(),
})
