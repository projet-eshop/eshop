import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Order } from "./Order";

@Entity()
export class Transport {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    pricePerWeight: number;

    @OneToMany(() => Order, order => order.transport)
    orders: Order[];
}
