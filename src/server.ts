import express from 'express';
import cors from 'cors';
import { userController } from './controller/userController';
import {configurePassport} from './utils/token'
import passport from 'passport';
import { orderController } from './controller/orderController';
import { productController } from './controller/productController';
import { commentController } from './controller/commentController';

configurePassport();
export const server = express();

server.use(express.json())
server.use(cors());

server.use(passport.initialize());

server.use(express.static('public'));

server.use('/api/user', userController)
server.use(express.static('public')); 


// server.use('/api/machin', machinController)
server.use('/api/order', orderController);
server.use('/api/product', productController)
server.use('/api/comment', commentController)

