import { Router } from "express";
import passport from "passport";
import { getRepository } from "typeorm";
import { Order } from "../entity/Order";
import { RowProduct } from "../entity/RowProduct";



export const orderController = Router();


orderController.get('/',async (rec,res)=>{
    try {
        
        const orders = await getRepository(Order).find({relations:['user']});

        res.json(orders);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});


orderController.post('/',passport.authenticate('jwt', {session:false}), async (req,res) => {
    try{

        let order = new Order();
        //Rajouter de la validation
        
        console.log(req.user.id);
        order.user = req.user;
        order.dateCrea = new Date();
        order.statusDelivered = 0;
        order.priceTotal = 0;
        order.rowProducts=[];
        if (req.user.shoppingCart?.length>0){
            
            for(let i=0; i<req.user.shoppingCart.length; i++){
                req.user.shoppingCart[i].user = null;
                order.rowProducts[i]=req.user.shoppingCart[i];
                order.priceTotal+=req.user.shoppingCart[i].price*req.user.shoppingCart[i].quantity;
            }
            await getRepository(Order).save(order);
            res.status(201).json(order);  
        }
        else{
            console.log("le panier est vide");
            res.status(404).end();
        }
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }

});

orderController.get('/:id', async(req, res)=>{
    try {
        const order = await getRepository(Order).findOne(req.params.id,{relations:['user','transport']});
        if(!order) {
            res.status(404).end();
            return;
        }
        res.json(order);
    
    } catch (error) {
        console.log(error);
            res.status(500).json(error);
    }
} )

orderController.delete('/:id',async(req, res)=>{
    try {
        const order = await getRepository(Order).delete(req.params.id);
        if (order.affected!==1){
            res.status(404).end();
            return;
        } 
        res.status(204).end();    
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})