import { Router } from "express";
import passport from "passport";
import { User, userSchema } from "../entity/User";
import { getRepository } from "typeorm";
import { generateToken } from "../utils/token";
import bcrypt from 'bcrypt'
import { RowProduct } from "../entity/RowProduct";
import { Product } from "../entity/Product";


export const userController = Router();

userController.get('/account', passport.authenticate('jwt', { session: false }), async (req, res) => {
    console.log(req.user);
    
    res.json(req.user)
});

userController.get('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const user = await getRepository(User).findOne(req.params.id, { relations: ["orders", "shoppingCart"] });
        if (!user) {
            return res.status(404).json({ message: 'utilisateur non trouvé' })
        }
        if (req.user.role == 'admin' || req.user.id === user.id) {
            return res.json(user)
        } return res.status(401).json({ message: 'erreur' })
    } catch (error) {
        console.log(error)
        res.status(400).json(error);
    }
})

userController.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    if (req.user.role == 'admin') {
        try {
            let users = await getRepository(User).find({ relations: ["orders"] })

            if (users) {
                return res.json(users)
            } return res.status(404).json({ message: "aucun utilisateurs" })
        } catch (error) {
            console.log(error);
            res.status(400).json(error);
        }
    } return res.status(401).json({ message: "vous n'avez pas les droits nécessaires" })

})

userController.post('/login', async (req, res) => {
    try {

        const { error } = userSchema.validate(req.body, { abortEarly: false});

        if (error) {
            res.status(400).json({ error: error.details.map(item => item.message) });
            return;
        }

        const user = await getRepository(User).findOne({ email: req.body.email });

        if (user) {
            let samePWD = await bcrypt.compare(req.body.password, user.password)
            if (samePWD) {
                res.json({
                    user,
                    loggedIn: true,
                    token: generateToken({
                        name: user.name,
                        email: user.email,
                        id: user.id,
                        role: user.role
                    })
                });
                console.log("user is ", user);

                return
            }
        }
        res.status(401).json({ loggedIn: false, error: ' Wrong email / password' });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

userController.post('/register', async (req, res) => {
    try {

        const { error } = userSchema.validate(req.body, { abortEarly: false});

        if (error) {
            res.status(400).json({ error: error.details.map(item => item.message) });
            return;
        }

        const newUser = new User();
        Object.assign(newUser, req.body);

        const exists = await getRepository(User).findOne({ email: newUser.email });

        if (exists) {
            res.status(400).json({ error: 'L_email est déjà pris' })
            return
        }
        newUser.role = 'user';
        console.log(newUser)

        newUser.password = await bcrypt.hash(newUser.password, 11);
        console.log(newUser.password)

        await getRepository(User).save(newUser);
        res.status(201).json({
            message: 'user created successfully',
            user: newUser,
            token: generateToken({
                email: newUser.email
            })
        });

        console.log(" new user is registered", newUser.firstName);


    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

 userController.post('/cart/:id', async (req, res) => {
    try {

        const product = await getRepository(Product).findOne(req.params.id);
        if (!product) {
            res.status(404).end();
            return;
        }

        const found= req.user.shoppingCart.find((rowProduct)=> product.id===rowProduct.product.id);

        if (found){

            found.quantity++;

            await getRepository(RowProduct).save(found);

            

        }

        else {

            const newProduct = new RowProduct;
            newProduct.user = req.user;
            newProduct.price = product.price;
            newProduct.quantity = 1;
            newProduct.product = product;

            await getRepository(RowProduct).save(newProduct);
            req.user.shoppingCart.push(newProduct);

        }

        res.status(200).json(req.user);



        
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});
   
        