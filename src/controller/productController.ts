import { Router } from "express";
import passport from "passport";
import { getRepository } from "typeorm";
import { Image } from "../entity/Image";
import { Product, productSchema } from "../entity/Product";
import { createThumbnail, uploader } from "../uploader";


export const productController = Router();

productController.get('/', async (req, res) => {

    try {



        const products = await getRepository(Product).find({ relations: ['images', 'comments'] });
        console.log(products)

        res.json(products);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})




productController.post('/', passport.authenticate('jwt', { session: false }), uploader.array('image', 10), async (req, res) => {
    try {
        const { error } = productSchema.validate(req.body, { abortEarly: false });

        if (error) {
            res.status(400).json({ error: error.details.map(item => item.message) });
            return;
        }

        if (req.user.role == 'admin') {
            let product = new Product();
            Object.assign(product, req.body);


            // console.log("req files are ", req.files['image']);
            product.images = await createThumbnail(req.files);

            await getRepository(Product).save(product);

            res.status(201).json(product);
        }
        else {return res.status(401).json({ message: "vous n'avez pas les droits nécessaires" })}
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }

});


productController.get('/:id', async (req, res) => {
    try {
        const product = await getRepository(Product).findOne(req.params.id, { relations: ['images', 'comments'] });
        if (!product) {
            res.status(404).end();
            return;
        }
        res.json(product);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

productController.patch('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        if (req.user.role == 'admin') {

            const repo = getRepository(Product);
            const product = await repo.findOne(req.params.id);
            if (!product) {
                res.status(404).end();
                return;
            }
            Object.assign(product, req.body);
            repo.save(product);

            res.json(product);
        }
        return res.status(401).json({ message: "vous n'avez pas les droits nécessaires" })
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})


productController.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        if (req.user.role == 'admin') {

            console.log(req.user.role);


            const repo = getRepository(Product);
            const result = await repo.delete(req.params.id);
            if (result.affected !== 1) {
                res.status(404).end();
                return;
            }
            return res.status(204).end();

        }

        return res.status(401).json({ message: "vous n'avez pas les droits nécessaires" })

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})
