import { Router } from "express";
import passport from "passport";
import { getRepository } from "typeorm";
import { Comment, commentSchema } from "../entity/Comment";
import { Product } from "../entity/Product";
import { createThumbnail, uploader } from "../uploader";

export const commentController = Router();



commentController.post('/:id', passport.authenticate('jwt', { session: false }), uploader.single('image'), async (req, res) => {
    console.log(req.user);

    try {
        let product = await getRepository(Product).findOne(req.params.id)
        if (product) {
            let comment = new Comment();
            Object.assign(comment, req.body);
            comment.date = new Date();
            comment.user = req.user
            comment.product = product
            await getRepository(Comment).save(comment);

            return res.status(201).json(comment);
        } 
        return res.json(404).end()







    } catch (error) {
        console.log(error);

        res.status(500).json(error);
    }

});


commentController.get('/:id', async (req, res) => {
    try {
        const comment = await getRepository(Comment).findOne(req.params.id, { relations: ['product', 'user'] });
        if (!comment) {
            res.status(404).end();
            return;
        }
        res.json(comment);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

commentController.patch('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        if (req.user.role == 'admin' || 'user') {

            const repo = getRepository(Comment);
            const comment = await repo.findOne(req.params.id);
            if (!comment) {
                res.status(404).end();
                return;
            }
            Object.assign(comment, req.body);
            repo.save(comment);

            res.json(comment);
        }

        return res.status(401).json({ message: "vous n'avez pas les droits nécessaires" })

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})


commentController.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        if (req.user.role == 'admin' || 'user') {

            const repo = getRepository(Comment);
            const result = await repo.delete(req.params.id);
            if (result.affected !== 1) {
                res.status(404).end();
                return;
            }

            res.status(204).end();
        }
        return res.status(401).json({ message: "vous n'avez pas les droits nécessaires" })

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})
