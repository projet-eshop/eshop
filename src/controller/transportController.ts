import { Router } from "express";
import { getRepository } from "typeorm";
import { Transport } from "../entity/Transport";

export const transportController = Router();


transportController.get('/', async(req, resp)=>{
    try {
        const transport = await getRepository(Transport).find({ relations: ['orders'] });
        resp.json(transport)
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);
        
    }
})

transportController.get('/:id', async(req, resp)=>{
    try {
        const transport = await getRepository(Transport).findOne(req.params.id, { relations: ['orders'] });
        if (!transport) {
            resp.status(404).end();
            return;
        }
        resp.json(transport)
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);
        
    }
})